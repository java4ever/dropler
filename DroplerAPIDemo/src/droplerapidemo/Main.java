/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package droplerapidemo;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import ua.com.codefire.dropler.net.bean.DropBeanUtil;
import ua.com.codefire.dropler.net.bean.FilesDrop;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        InetAddress address = InetAddress.getByName("192.168.8.102");
        List<File> fileList = Arrays.asList(new File("/Users/human/Projects/Dropler/DroplerClient/dist").listFiles());

        File storeFile = new File("store.xml");

        FileOutputStream fileOutputStream = new FileOutputStream(storeFile);
        FilesDrop filesDrop = new FilesDrop(address, fileList);

        DropBeanUtil.storeToStream(fileOutputStream, filesDrop);

        Socket socket = new Socket();
        socket.connect(DropBeanUtil.getDefaultDroplerAddress(), 3000);

        if (socket.isConnected()) {
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(storeFile.getAbsolutePath());
            dos.flush();
        }
        
//        ZipFile zipFile = new ZipFile("./rcv/nbproject.zip");
//        Enumeration<? extends ZipEntry> entries = zipFile.entries();
//        
//        while (entries.hasMoreElements()) {
//            System.out.println(entries.nextElement().getName());
//        }
    }
}
